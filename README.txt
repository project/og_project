
This module is required if you enable both Organic groups (OG) and the
project module.  Otherwise, projects (and especially the issue
tracker) will not work with organic groups.

Send feature requests and bug reports to the issue tracking system for
this module: http://drupal.org/node/add/project-issue/og_project.

Author: Derek Wright -- http://drupal.org/user/46549 ("dww")

